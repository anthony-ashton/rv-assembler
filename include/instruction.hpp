/**
 *  Copyright (c) 2018-2019 Jesse Buhagiar
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 * instruction.hpp
 *
 *  Created on: 14 Nov. 2018
 *      Author: Jesse Buhagiar
 */

#ifndef INCLUDE_INSTRUCTION_HPP_
#define INCLUDE_INSTRUCTION_HPP_

#include <cstdint>
#include <string>

#include "common.hpp"

/**
 * RISC-V has quite a few types of instruction formats. They are (in no particular order):
 *
 *  � Stored-Program Concept
 *  � R-Format:                 instructions using 3 register inputs
 *  � I-Format:                 instructions with immediates, loads
 *  � S-Format:                 store instructions: sw, sb
 *  � SB-Format:                branch instructions: beq, bge
 *  � U-Format:                 instructions with upper immediates
 *  � UJ-Format:                jump instructions: jal
 *
 *  These are each 32-bits wide
 *
 *  The following table describes the registers and their corresponding ABI names:
 *
 *  -----------------------------------------------------------
 *  | Register | ABI NAME |   Saver  |       Description      |
 *  |    x0    |   zero   |   ---    |    Hard Wired Zero     |
 *  |    x1    |    ra    |  Caller  |     Return Address     |
 *  |    x2    |    sp    |  Callee  |     Stack Pointer      |
 *  |    x3    |    gp    |   ---    |     Global Pointer     |
 *  |    x4    |    tp    |   ---    |     Thread Pointer     |
 *  |  x5-x7   |   t0-t2  |  Caller  |      Temporaries       |
 *  |    x8    |   s0/fp  |  Callee  | Saved Reg/Frame Pointer|
 *  |    x9    |    s1    |  Callee  |     Saved Register     |
 *  | x10-x11  |   a0-a1  |  Caller  |Func. Arguments/Ret vals|
 *  | x12-x17  |   a2-a7  |  Caller  |   Function Arguments   |
 *  | x18-x27  |   s2-s11 |  Callee  |     Saved Registers    |
 *  | x28-x31  |   t3-t6  |  Caller  |      Temporaries       |
 *  |---------------------------------------------------------|
 *  |   f0-f7  |  ft0-ft7 |  Caller  |     FP Temporaries     |
 *  |   f8-f9  |  fs0-fs1 |  Callee  |     FP Saved Regs      |
 *  |  f10-f11 |  fa0-fa1 |  Caller  | FP Arguments/Ret. vals |
 *  |  f12-f17 |  fa2-f7  |  Caller  |     FP Arguments       |
 *  |  f18-f27 |  fs2-f11 |  Callee  |   FP Saved Registers   |
 *  |  f28-f31 |  ft8-f11 |  Caller  |     FP Temporaries     |
 *  -----------------------------------------------------------
 *
 */

// TODO: Move these to an enum???
static constexpr uint8_t R_OPCODE   = 0x33;
static constexpr uint8_t I_OPCODE   = 0x13;
static constexpr uint8_t S_OPCODE   = 0x23;
static constexpr uint8_t SB_OPCODE  = 0x63;
static constexpr uint8_t CSR_OPCODE = 0x73;



// This union should be naturally aligned automatically, as it's 32-bits wide!
// TODO: Should opcode be moved to outside the union?
typedef union
{
    // opcode and funct3+funct7 (10 bits) describe what operation to perform
    // R type instructions have opcode 0b0110011
    struct r_type
    {
        std::uint8_t opcode : 7;    // Operation Code
        std::uint8_t rd : 5;        // Destination register (this is a number from 0 to 31)
        std::uint8_t funct3 : 3;
        std::uint8_t rs1 : 5;       // Source register 1 (this is a number from 0 to 31)
        std::uint8_t rs2 : 5;       // Source register 2 (this is a number from 0 to 31)
        std::uint8_t funct7 : 7;
    } __attribute__((packed)) r_type;

    // Immediate Instruction Type
    struct i_type
    {
        std::uint8_t opcode : 7;    // Operation Code
        std::uint8_t rd : 5;
        std::uint8_t func3 : 3;
        std::uint8_t rs1 : 5;
        std::uint16_t imm : 12;
    } __attribute__((packed)) i_type;

    // Store instructions
    struct s_type
    {
        std::uint8_t opcode : 7;    // Operation Code
        std::uint8_t imm5 : 5;
        std::uint8_t func3 : 3;
        std::uint8_t rs1 : 5;
        std::uint8_t rs2 : 5;
        std::uint8_t imm7 : 7;
    } __attribute__((packed)) s_type;

    // Branch instruction (SB)
    // The encoding for the immediate is absolutely fucked.
    // Please make sure you emit good code for this instruction type!
    struct sb_type
    {
        std::uint8_t opcode : 7;    // Operation Code
        std::uint8_t imm11 : 1;
        std::uint8_t imm4_1 : 4;
        std::uint8_t func3 : 3;
        std::uint8_t rs1 : 5;       // Source register 1
        std::uint8_t rs2 : 5;       // Source register 2
        std::uint8_t imm10_5 : 6;
        std::uint8_t imm12 : 1;
    } __attribute__((packed)) sb_type;

    // U type instruction
    // This loads the upper 20 bits of the register rd, and clears the lower 12. For example:
    // LUI x10, 0x87654 -----> x10 = 0x87654000
    struct u_type
    {
        std::uint8_t opcode : 7;    // Operation Code
        uint8_t rd : 5;             // Destination register
        uint32_t imm : 20;          // Bits 31:12 of immediate
    } __attribute__((packed)) u_type;

    // J type instructions (for jumps, jal, j etc)
    struct uj_type
    {
        std::uint8_t opcode : 7;    // Operation Code
        std::uint8_t rd : 5;
        std::uint8_t imm19_12 : 8;
        std::uint8_t imm11 : 1;
        std::uint16_t imm10_1 : 10;
        std::uint8_t imm20 : 1;
    } __attribute__((packed)) uj_type;

} __attribute__((packed)) instruction_t;

/**
 * Instruction Mnemonic (for table lookup)
 */
struct CInstruction final
{
public:
    /**
     * Instruction type
     */
    enum class InstructionType
    {
        R_TYPE,
        I_TYPE,
        S_TYPE,
        SB_TYPE,
        U_TYPE,
        UJ_TYPE
    };

    static constexpr unsigned int INSTRUCTION_SIZE = 4; /**< Each instruction is 4-bytes long */

public:
    CInstruction(const std::string& _mnemonic = "",
                 const std::uint8_t _opcode = 0x00,
                 InstructionType _type = InstructionType::I_TYPE,
                 const std::uint8_t _noperands = 0x00,
                 const std::uint8_t _funct3 = 0x00,
                 const std::uint8_t _funct7 = 0x00
                )
        : opcode(_opcode), mnemonic(_mnemonic), funct3(_funct3), funct7(_funct7), type(_type), noperands(_noperands)
        {
        }

    /**
     * Comparison operator for std::find
     */
    friend bool operator== (const CInstruction& lhs, const CInstruction& rhs)
    {
        return lhs.mnemonic == rhs.mnemonic;
    }

    /**
     * Get the mnemonic for this instruction
     */
    const __forceinline std::string& GetMnemonic() const{return mnemonic;}

    /**
     *  Get the opcode of this mnemonic.
     */
    const __forceinline std::uint8_t& GetOpcode() const{return opcode;}

    /**
     * Get the value of 'funct3' for this Instruction
     */
    const __forceinline std::uint8_t& GetFunct3() const{return funct3;}

    /**
     * Get the value of 'funct7' for this Instruction
     */
    const __forceinline std::uint8_t& GetFunct7() const{return funct7;}

    /**
     * Get the number of operands for this line
     */
   const __forceinline int& GetNumOperands() const{return noperands;}

    /**
     * Get the format of this instruction
     */
    const __forceinline InstructionType& GetFormat() const{return type;}

private:
    std::uint8_t    opcode;         /**< Opcode for this instruction */
    std::string     mnemonic;       /**< The mnemonic for this instruction */
    std::uint8_t    funct3;         /**< 3-bit function code */
    std::uint8_t    funct7;         /**< 7-bit function code */

    InstructionType type;           /**< Instruction format type (to determine what union to access at assemble time) */

    int             noperands;      /**< Number of operands this instruction should have (e.g, 'and rd,rs1,rs2' has 3 operands!) */
};

#endif /* INCLUDE_INSTRUCTION_HPP_ */
