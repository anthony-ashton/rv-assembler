/**
 *  Copyright (c) 2018-2019 Jesse Buhagiar
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 * register.hpp
 *
 *  Created on: 20 Nov. 2018
 *      Author: Jesse Buhagiar
 */

#ifndef INCLUDE_REGISTER_HPP_
#define INCLUDE_REGISTER_HPP_

#include <string>
#include <cstdint>

#include "common.hpp"

struct CRegister final
{
public:
    /**
     * No default constructor
     */
    CRegister() = delete;

    /**
     * Construct a mnemonic/value pair
     */
    CRegister(const std::string& mnemonic, const std::uint8_t _val)
        : reg(mnemonic), val(_val)
    {

    }

    /**
     * Comparison operator for std::find
     */
    friend bool operator== (const CRegister& lhs, const CRegister& rhs)
    {
        return lhs.reg == rhs.reg;
    }

    /**
     * Get the mnemonic for this instruction
     */
    const __forceinline std::string& GetRegname() const{return reg;}

    /**
     *  Get the opcode of this mnemonic.
     */
    const __forceinline std::uint8_t& GetRegVal() const{return val;}

private:
    std::string     reg;
    std::uint8_t    val;
};






#endif /* INCLUDE_REGISTER_HPP_ */
