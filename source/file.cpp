/**
 *  Copyright (c) 2018-2019 Jesse Buhagiar
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 * file.cpp
 *
 *  Created on: 25 Oct. 2018
 *      Author: Jesse Buhagiar
 */
#include "file.hpp"
#include <iostream>

#include "boost/tokenizer.hpp"

CFile::CFile(const std::string& path)
    : fhandle(), fname(path), lines(), nlines(0)
{
}

CFile::OpenStatus CFile::Read(const std::string fname)
{
    std::string line;

    fhandle.open(fname);
    if(!fhandle.is_open())              // We couldn't open this file for some reason!? Perhaps it doesn't exist!
        return OpenStatus::IO_ERROR;

    // At this point, the file is now open for business!
    // All we're doing is just getting the file line by line, with no parsing
    while(std::getline(fhandle, line))
    {
        //if(line.size() == 0) // Ignore any blank lines
            //continue;

        lines.push_back(line);
        nlines++;
    }

    return OpenStatus::SUCCESS;
}

void CFile::RemoveLine(const size_t line)
{
    if(line >= GetNumberOfLines())
        return;

    lines.erase(lines.begin(), lines.begin() + line);
}

std::vector<std::string> CFile::SplitLine(const size_t line) const
{
    std::vector<std::string> ret;
    const std::string& sline = lines.at(line);

    boost::char_separator<char> sep(", ");
    boost::tokenizer<boost::char_separator<char>> tok(sline, sep);
    for(const auto& t : tok)
    {
        ret.push_back(t);
    }

    return ret;
}

const std::string CFile::GetName() const
{
    if(fname.find("/") != std::string::npos)
    {
        size_t start = fname.find_last_of("/");
        return std::string(fname.substr(start, fname.npos - start));
    }
    else if(fname.find("\\") != std::string::npos)
    {
        size_t start = fname.find_last_of("\\");
        return std::string(fname.substr(start, fname.npos - start));
    }

    return fname;
}
