/**
 *  Copyright (c) 2018-2019 Jesse Buhagiar
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 * assembler.hpp
 *
 *  Created on: 14 Nov. 2018
 *      Author: Jesse Buhagiar
 */

#ifndef INCLUDE_ASSEMBLER_HPP_
#define INCLUDE_ASSEMBLER_HPP_

#include "common.hpp"
#include "file.hpp"
#include "instruction.hpp"
#include "register.hpp"

#include <vector>
#include <map>
#include <string>

/**
 * Main Assembler Class
 *
 * NOTE: THIS CLASS IS A SINGLETON! IT SHOULD ONLY AND CAN ONLY BE INSTANTIATED ONCE!!!
 */
class CAssembler
{
public:
    CAssembler();

    /**
     * Get the instance of this assembler
     */
    static __forceinline CAssembler& GetInstance()
    {
        static CAssembler assembler;

        return assembler;
    }

    /**
     * Sets the filename that we want to be assembled into a binary
     */
    void __forceinline SetFileToAssemble(const std::string& path){
        file.SetFilePath(path);
        file.Read(path);
    }

    /**
     * Assemble the given file
     */
    void Assemble();

private:
    /**
     * Wrapper for finding registers
     */
    std::uint8_t FindRegister(const std::string& reg);

    /**
     * Invokes the preprocessor (first pass before assembler)
     */
    void Preprocess(void);

    /**
     * Helper function to parse immediate values (and to make code compatible with GNU AS)
     */
    void ParseImmediate(const std::string& imm);

    /**
     * Print an error to the screen, as well as setting the syntaxError flag
     */
    void PrintError(size_t line, const std::string& message);

private:
    CFile file; /**< File that we actually want to assemble */

    std::vector<CInstruction>                   instructions;   /**< Table of instructions */
    std::vector<instruction_t>                  program;
    std::map<std::string, std::uint8_t>         registers;      /**< Register value table */
    std::map<std::string, std::uint64_t>        labels;         /**< Label table */

    std::map<std::string, size_t>               constants;      /**< Constants map (for .equ) */

    std::uint64_t                               currentAddr;    /** Keep track of current address we want to write code to */

    bool                                        syntaxError;    /** Keep track of whether or not we've detected a syntax error */

};



#endif /* INCLUDE_ASSEMBLER_HPP_ */
