/**
 *  Copyright (c) 2018-2019 Jesse Buhagiar
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 * args.hpp: Assembler arguments parser
 *
 *  Created on: 9 Nov. 2018
 *      Author: Jesse Buhagiar
 */
#ifndef INCLUDE_ARGS_HPP_
#define INCLUDE_ARGS_HPP_

#include <string>
#include <vector>

class CArgs{
private:
    enum class FlagType : int
    {
        CODE_FLAG = 0,
        WARN_FLAG,
    };

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
    CArgs() = delete; // Force constructor
    CArgs(int _argc, char** _argv);

    /**
     *  Parse all input arguments and dispatch appropriate jobs
     */
    void Parse();

    /**
     * Get the status of an argument flag
     */
    inline bool GetFlagEnabled(FlagType type, std::uint64_t& flagbit) const;

private:
    int                         argc;       /**< Number of command line args passed to program */
    std::vector<std::string>    argv;       /**<  Safer, c++ arguments vector */

    std::string                 filename;   /**< Path of the file we want to assemble! */


    std::uint64_t               genFlags;   /**< Flags related to code generation */
    std::uint64_t               warnFlags;  /**< Flags related to warnings */
};









#endif
