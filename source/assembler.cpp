/**
 *  Copyright (c) 2018-2019 Jesse Buhagiar
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 * assembler.cpp
 *
 *  Created on: 14 Nov. 2018
 *      Author: Jesse Buhagiar
 */
#include "alphanumerical.hpp"
#include "assembler.hpp"
#include "register.hpp"
#include "file.hpp"

#include <iostream>
#include <algorithm>
#include <cassert>

using InstructionType = CInstruction::InstructionType;

CAssembler::CAssembler()
{

    // Map Registers
    registers.insert(std::pair<std::string, std::uint8_t>("x0", 0));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x1", 1));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x2", 2));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x3", 3));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x4", 4));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x5", 5));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x6", 6));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x7", 7));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x8", 8));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x9", 9));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x10", 10));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x11", 11));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x12", 12));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x13", 13));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x14", 14));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x15", 15));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x16", 16));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x17", 17));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x18", 18));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x19", 19));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x20", 20));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x21", 21));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x22", 22));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x23", 23));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x24", 24));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x25", 25));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x26", 26));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x27", 27));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x28", 28));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x29", 29));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x30", 30));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("x31", 31));// @suppress("Invalid arguments")

    registers.insert(std::pair<std::string, std::uint8_t>("zero", 0));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("ra", 1));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("sp", 2));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("gp", 3));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("tp", 4));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("t0", 5));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("t1", 6));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("t2", 7));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s0", 8));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("fp", 8));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s1", 9));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("a0", 10));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("a1", 11));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("a2", 12));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("a3", 13));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("a4", 14));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("a5", 15));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("a6", 16));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("a7", 17));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s2", 18));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s3", 19));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s4", 20));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s5", 21));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s6", 22));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s7", 23));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s8", 24));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s9", 25));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s10", 26));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("s11", 27));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("t3", 28));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("t4", 29));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("t5", 30));// @suppress("Invalid arguments")
    registers.insert(std::pair<std::string, std::uint8_t>("t6", 31));// @suppress("Invalid arguments")




    // Fill instruction table.
    // R32I Base Instructions (Integer)
    instructions.push_back(CInstruction("add",      0b0110011, InstructionType::R_TYPE, 3, 0b000));
    instructions.push_back(CInstruction("addi",     0b0010011, InstructionType::I_TYPE, 3, 0b000));
    instructions.push_back(CInstruction("and",      0b0110011, InstructionType::R_TYPE, 3, 0b111));
    instructions.push_back(CInstruction("andi",     0b0010011, InstructionType::I_TYPE, 3, 0b111));
    instructions.push_back(CInstruction("auipc",    0b0010111, InstructionType::U_TYPE, 2)); // Add upper immediate to PC
    instructions.push_back(CInstruction("beq",      0b1100011, InstructionType::SB_TYPE, 3, 0b00));
    instructions.push_back(CInstruction("bge",      0b1100011, InstructionType::SB_TYPE, 3, 0b101));
    instructions.push_back(CInstruction("bgeu",     0b1100011, InstructionType::SB_TYPE, 3, 0b111));
    instructions.push_back(CInstruction("blt",      0b1100011, InstructionType::SB_TYPE, 3, 0b100));
    instructions.push_back(CInstruction("bltu",     0b1100011, InstructionType::SB_TYPE, 3, 0b110));
    instructions.push_back(CInstruction("bne",      0b1100011, InstructionType::SB_TYPE, 3, 0b001));
    instructions.push_back(CInstruction("csrrc",    0b1110011, InstructionType::I_TYPE, 3, 0b011));
    instructions.push_back(CInstruction("csrrci",   0b1110011, InstructionType::I_TYPE, 3, 0b111));
    instructions.push_back(CInstruction("csrrs",    0b1110011, InstructionType::I_TYPE, 3, 0b010));
    instructions.push_back(CInstruction("csrrw",    0b1110011, InstructionType::I_TYPE, 3, 0b001));
    instructions.push_back(CInstruction("csrrwi",   0b1110011, InstructionType::I_TYPE, 3, 0b101));
    instructions.push_back(CInstruction("ebreak",   0b1110011, InstructionType::I_TYPE, 0, 0b000));
    instructions.push_back(CInstruction("ecall",    0b1110011, InstructionType::I_TYPE, 0, 0b000));
    instructions.push_back(CInstruction("fence",    0b0001111, InstructionType::I_TYPE, 0, 0b000));
    instructions.push_back(CInstruction("fence.i",  0b0001111, InstructionType::I_TYPE, 0, 0b001));
    instructions.push_back(CInstruction("jal",      0b1101111, InstructionType::UJ_TYPE, 2));
    instructions.push_back(CInstruction("jalr",     0b1100111, InstructionType::UJ_TYPE, 3, 0b000));
    instructions.push_back(CInstruction("lb",       0b0000011, InstructionType::I_TYPE, 3, 0b000));
    instructions.push_back(CInstruction("lbu",      0b0000011, InstructionType::I_TYPE, 3, 0b100));
    instructions.push_back(CInstruction("lh",       0b0000011, InstructionType::I_TYPE, 3, 0b001));
    instructions.push_back(CInstruction("lhu",      0b0000011, InstructionType::I_TYPE, 3, 0b101));
    instructions.push_back(CInstruction("lui",      0b0110111, InstructionType::U_TYPE, 2));
    instructions.push_back(CInstruction("lw",       0b0000011, InstructionType::I_TYPE, 3, 0b010));
    instructions.push_back(CInstruction("or",       0b0110011, InstructionType::R_TYPE, 3, 0b110));
    instructions.push_back(CInstruction("ori",      0b0010011, InstructionType::I_TYPE, 3, 0b110));
    instructions.push_back(CInstruction("sb",       0b0100011, InstructionType::S_TYPE, 3, 0b000));
    instructions.push_back(CInstruction("sh",       0b0100011, InstructionType::S_TYPE, 3, 0b001));
    instructions.push_back(CInstruction("sll",      0b0110011, InstructionType::R_TYPE, 3, 0b001));
    instructions.push_back(CInstruction("slt",      0b0110011, InstructionType::R_TYPE,3,  0b010));
    instructions.push_back(CInstruction("slti",     0b0010011, InstructionType::I_TYPE, 3, 0b010));
    instructions.push_back(CInstruction("sltiu",    0b0010011, InstructionType::I_TYPE, 3, 0b011));
    instructions.push_back(CInstruction("sltu",     0b0110011, InstructionType::R_TYPE, 3, 0b011));
    instructions.push_back(CInstruction("sra",      0b0110011, InstructionType::R_TYPE, 3, 0b101));
    instructions.push_back(CInstruction("srl",      0b0110011, InstructionType::R_TYPE, 3, 0b101));
    instructions.push_back(CInstruction("sub",      0b0110011, InstructionType::R_TYPE, 3, 0b000));
    instructions.push_back(CInstruction("sw",       0b0100011, InstructionType::S_TYPE, 3, 0b010));
    instructions.push_back(CInstruction("xor",      0b0110011, InstructionType::R_TYPE, 3, 0b100));
    instructions.push_back(CInstruction("xori",     0b0010011, InstructionType::I_TYPE, 3, 0b100));

    // R64I Base Instructions (Integer)
    instructions.push_back(CInstruction("lwu",      0b0000011, InstructionType::I_TYPE, 0b110));
    instructions.push_back(CInstruction("ld",       0b0000011, InstructionType::I_TYPE, 0b011));
    instructions.push_back(CInstruction("sd",       0b0100011, InstructionType::S_TYPE, 0b011));
    instructions.push_back(CInstruction("slli",     0b0010011, InstructionType::I_TYPE, 0b001, 0b0000000));
    instructions.push_back(CInstruction("slliw",    0b0011011, InstructionType::I_TYPE, 0b001, 0b0000000));
    instructions.push_back(CInstruction("srli",     0b0010011, InstructionType::I_TYPE, 0b101, 0b0000000));
    instructions.push_back(CInstruction("srliw",    0b0011011, InstructionType::I_TYPE, 0b101, 0b0000000));
    instructions.push_back(CInstruction("srai",     0b0010011, InstructionType::I_TYPE, 0b101, 0b010000));
    instructions.push_back(CInstruction("addiw",    0b0011011, InstructionType::I_TYPE, 0b000));
    instructions.push_back(CInstruction("sraiw",    0b0011011, InstructionType::I_TYPE, 0b101, 0b0100000));
    instructions.push_back(CInstruction("addw",     0b0111011, InstructionType::R_TYPE, 0b000, 0b0000000));
    instructions.push_back(CInstruction("subw",     0b0111011, InstructionType::R_TYPE, 0b000, 0b0100000));
    instructions.push_back(CInstruction("sllw",     0b0111011, InstructionType::R_TYPE, 0b001, 0b0000000));
    instructions.push_back(CInstruction("srlw",     0b0111011, InstructionType::R_TYPE, 0b101, 0b0000000));
    instructions.push_back(CInstruction("sraw",     0b0111011, InstructionType::R_TYPE, 0b101, 0b0100000));


    currentAddr = 0;
    syntaxError = false;
}

std::uint8_t CAssembler::FindRegister(const std::string& reg)
{
    std::map<std::string, std::uint8_t>::const_iterator it;

    it = registers.find(reg);
    if(it == registers.end())
    {
        std::cout << file.GetName() << ": Error: unknown register name " << reg << std::endl;
        syntaxError = true;
        return 0xFF;
    }

    return registers.at(reg);
}

void CAssembler::Preprocess()
{
    for(size_t i = 0; i < file.GetNumberOfLines(); i++)
    {
        const std::string& line = file.GetLine(i);

        if(line.size() == 0)
            continue;

        if(line.at(0) != '.')
            continue;

        std::vector<std::string> ssplit = file.SplitLine(i);
        if(ssplit.at(0) == ".equ")
        {
            if(ssplit.size() > 3)
                std::cout << file.GetName() << ": Warning: junk after constant definition; ignoring..." << std::endl;

            constants[ssplit.at(0)] = Str2Num<size_t>(ssplit.at(2));
        }
    }
}

void CAssembler::ParseImmediate(const std::string& imm)
{

}

void CAssembler::Assemble()
{
    //TODO: Some kind of macro/preprocessing here!
    Preprocess();

    // Assemble code...
    for(size_t i = 0; i < file.GetNumberOfLines(); i++)
    {
        const std::string& line = file.GetLine(i);
        std::vector<std::string> ssplit = file.SplitLine(i);

        if(line.size() == 0)
            continue;

        if(line.at(0) == '.') // Ignore any and all preprocessor/directive stuff
            continue;

        // Ignore comments
        if(line.at(0) == '#' || line.compare(0, 2, "//") == 0)
            continue;

        // Check if a line is actually a label (or contains one)
        if(ssplit.at(0).at(ssplit.at(0).size() - 1) == ':')
        {
            // TODO: HANDLE ME!!!!
            //if(ssplit.size() > 1) // Someone's been naughty and put code on the same line as the label! Handle this please
                //continue;

            continue;
        }

        // Assemble line....
        std::string mnemonic = ssplit.at(0);
        std::vector<CInstruction>::iterator it = std::find(std::begin(instructions), std::end(instructions), mnemonic);
        CInstruction inst;
        instruction_t instruction;
        if(it != instructions.end())
        {
            auto index = std::distance(instructions.begin(), it);
            inst = instructions.at(index);
        }

        // Make sure the instruction has the correct number of operands, otherwise
        // give the user an error and stop assembling.
        if(ssplit.size() - 1 < inst.GetNumOperands())
        {
            std::cout << file.GetName() << ": Error: invalid number of operands for instruction " << ssplit.at(0) << std::endl;
            syntaxError = true;
            continue;
        }

        // Do instruction encoding
        if(inst.GetFormat() == InstructionType::R_TYPE)
        {
            instruction.r_type.opcode = inst.GetOpcode();
            std::uint8_t rd  = FindRegister(ssplit.at(1));
            std::uint8_t rs1 = FindRegister(ssplit.at(2));
            std::uint8_t rs2 = FindRegister(ssplit.at(3));

            instruction.r_type.rd     = rd;
            instruction.r_type.rs1    = rs1;
            instruction.r_type.rs2    = rs2;
            instruction.r_type.funct3 = inst.GetFunct3();
            instruction.r_type.funct7 = inst.GetFunct7();
        }
        else if(inst.GetFormat() == InstructionType::I_TYPE)
        {
            instruction.i_type.opcode = inst.GetOpcode();
            std::uint8_t rd     = FindRegister(ssplit.at(1));
            std::uint8_t rs1    = FindRegister(ssplit.at(2));
            std::uint16_t imm;

            // Determine whether immediate is base 16 or base 10
            if(std::isalpha(ssplit.at(3).at(0)))
            {
                // TODO: Label look up!
            }
            else if(isHexNumber(ssplit.at(3)))
            {
                imm = std::stol(ssplit.at(3), NULL, 16);
            }
            else
            {
                imm = std::stol(ssplit.at(3));
            }

            // Check for immediate limits
            if(imm > 0xfff)
            {
                std::cout << "Error: Immediate value limits exceeded!";
                return;
            }

            instruction.i_type.rd     = rd;
            instruction.i_type.rs1    = rs1;
            instruction.i_type.func3  = inst.GetFunct3();
            instruction.i_type.imm    = imm;
        }
        else if(inst.GetFormat() == InstructionType::U_TYPE)
        {
            instruction.u_type.opcode = inst.GetOpcode();
            std::uint8_t rd     = FindRegister(ssplit.at(1));
            std::uint32_t imm;

            // Determine whether immediate is base 16 or base 10
            if(std::isalpha(ssplit.at(2).at(0)))
            {
                // TODO: Label look up!
            }
            else if(isHexNumber(ssplit.at(2)))
            {
                imm = std::stol(ssplit.at(2), NULL, 16);
            }
            else
            {
                imm = std::stol(ssplit.at(2));
            }

            // Check for immediate limits
            if(imm > 0xfffff)
            {
                std::cout << file.GetName() << ": Error: immediate value limits exceeded!";
                return;
            }

            instruction.u_type.rd   = rd;
            instruction.u_type.imm  = imm;
        }
        else if(inst.GetFormat() == InstructionType::S_TYPE)
        {
            instruction.s_type.opcode = inst.GetOpcode();
            std::uint8_t rs1 = FindRegister(ssplit.at(1));
            std::uint8_t rs2 = FindRegister(ssplit.at(2));
            std::uint16_t imm;

            // Determine whether immediate is base 16 or base 10
            if(std::isalpha(ssplit.at(3).at(0)))
            {
                // TODO: Label look up!
            }
            else if(isHexNumber(ssplit.at(3)))
            {
                imm = std::stol(ssplit.at(3), NULL, 16);
            }
            else
            {
                imm = std::stol(ssplit.at(3));
            }

            instruction.s_type.rs1      = rs1;
            instruction.s_type.rs2      = rs2;
            instruction.s_type.func3    = inst.GetFunct3();
            instruction.s_type.imm5     = (imm & 0x1F);
            instruction.s_type.imm7     = (imm & 0xFE0) >> 5;
        }
        else if(inst.GetFormat() == InstructionType::SB_TYPE)
        {
            instruction.sb_type.opcode = inst.GetOpcode();
            std::uint8_t rs1 = FindRegister(ssplit.at(1));
            std::uint8_t rs2 = FindRegister(ssplit.at(2));
            std::uint16_t addr;

            // Determine whether immediate is base 16 or base 10
            if(std::isalpha(ssplit.at(3).at(0)))
            {
                // TODO: Label look up!
            }
            else if(isHexNumber(ssplit.at(3)))
            {
                addr = std::stol(ssplit.at(3), NULL, 16);
            }
            else
            {
                addr = std::stol(ssplit.at(3));
            }

            instruction.sb_type.rs1 = rs1;
            instruction.sb_type.rs2 = rs2;
            instruction.sb_type.func3 = inst.GetFunct3();

            // TODO: Revise this to make sure it's 100% correct!
            instruction.sb_type.imm4_1  = (addr & 0x1E) >> 1;
            instruction.sb_type.imm11   = (addr & 0x800) >> 11;
            instruction.sb_type.imm12   = (addr & 0x1000) >> 12;
            instruction.sb_type.imm10_5 = (addr & 0x7E0) >> 5;
        }
        else if(inst.GetFormat() == InstructionType::UJ_TYPE)
        {
            instruction.uj_type.opcode = inst.GetOpcode();
            std::uint8_t rd = FindRegister(ssplit.at(1));
            std::uint16_t addr;

            // Determine whether immediate is base 16 or base 10
            if(std::isalpha(ssplit.at(inst.GetNumOperands()).at(0)))
            {
                // TODO: Label look up!
            }
            else if(isHexNumber(ssplit.at(inst.GetNumOperands())))
            {
                addr = std::stol(ssplit.at(inst.GetNumOperands()), NULL, 16);
            }
            else
            {
                addr = std::stol(ssplit.at(inst.GetNumOperands()));
            }

            instruction.uj_type.rd = rd;
            instruction.uj_type.imm11       = (addr & 0x800) >> 11;
            instruction.uj_type.imm20       = (addr & 0x100000) >> 20;
            instruction.uj_type.imm19_12    = (addr & 0xFF000) >> 12;
            instruction.uj_type.imm10_1     = (addr & 0x7FE) >> 1;
        }
        program.push_back(instruction);

        if(!syntaxError)
        {
            std::ofstream prog("test.bin", std::ios::binary | std::ios::out);
            prog.write(reinterpret_cast<const char*>(&program[0]), sizeof(instruction) * program.size());

            prog.close();
        }
    }
}
