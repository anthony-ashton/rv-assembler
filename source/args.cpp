/**
 *  Copyright (c) 2018-2019 Jesse Buhagiar
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 * args.cpp
 *
 *  Created on: 9 Nov. 2018
 *      Author: Jesse Buhagiar
 */
#include "args.hpp"
#include "assembler.hpp"

#include <iostream>
#include <stdexcept>
#include <cstdlib>

CArgs::CArgs(int _argc, char** _argv)
    : argc(_argc), argv(), filename(""), genFlags(0x0), warnFlags(0x0)
{
    argv.reserve(argc);
    argv.assign(_argv, _argv + _argc); // Copy data into the arguments list
}

void PrintVersionInfo()
{
    std::cout << "The RMIT RISC-V Assembler. Built on " << __DATE__ << " at " << __TIME__ << std::endl;
    std::cout << "Copyright (C) 2018-2019 Jesse Buhagiar" << std::endl;
    std::cout << "This is free software; see the source for copying conditions. There is NO warranty!" << std::endl;
    std::exit(EXIT_SUCCESS);
}

void PrintHelp()
{
    std::cout << "Help goes here!\n";
    std::exit(EXIT_SUCCESS);
}

bool assemble = false;
void CArgs::Parse()
{
    if(argc <= 1)
    {
        std::cout << "rvasm: fatal error: no input files detected!" << std::endl;
        std::cout << "For help, try rvasm.exe --help" << std::endl;
        std::exit(EXIT_SUCCESS);
    }

    // Walk the arguments list
    for(std::vector<std::__cxx11::basic_string<char> >::size_type i = 1; i < argv.size(); i++) // NOTE: Shut the compiler up about signed comparison bullshit.
    {
        std::string& arg = argv.at(i); // Get the current argument

        if(arg.at(0) != '-')
        {
            std::cerr << "Invalid argument " << arg << std::endl;
            std::exit(EXIT_FAILURE);
        }
        else
        {
            if(arg.at(1) == '-')
            {
                if(arg.compare(2, arg.size() - 1, "version") == 0)
                {
                    PrintVersionInfo();
                }
            }

            // Arguments like -h -c etc
            if(arg.size() == 2)
            {
                switch(arg.at(1))
                {
                case 'c':
                    try
                    {
                        filename = argv.at(i + 1);
                        CAssembler::GetInstance().SetFileToAssemble(filename);
                        assemble = true;
                        i++; // Make sure filename isn't treated as an argument
                    }
                    catch(std::out_of_range& ex)
                    {
                        std::cout << "No input file detected! Aborting..." << std::endl;
                        std::exit(EXIT_FAILURE);
                    }
                    break;
                case 'v':
                    PrintVersionInfo();
                    break;
                case 'h':
                    PrintHelp();
                    break;
                default:
                    std::cout << "Unknown flag " << arg.at(1) << "! Ignoring..." << std::endl;
                    break;
                }
            }
        }
    }

    // TODO: Some kind of 'assembler' object here!?! We can guarantee that we have a filename at this point!
    CAssembler::GetInstance().Assemble();

}

bool CArgs::GetFlagEnabled(CArgs::FlagType type, std::uint64_t& flagbit) const
{
    switch(type)
    {
    case FlagType::CODE_FLAG:
        return genFlags & (1 << flagbit);
        break;
    case FlagType::WARN_FLAG:
        return warnFlags & (1 << flagbit);
        break;
    default:
        std::cerr << "Invalid flag type! Ignoring...";
        break;
    }

    return false;
}
